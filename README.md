# Weather application for buildit @ wipro digital

This is a demo application, which shows the current weather and a weather forecast for the user location. The location is obtained through GeoLocation API, or- if not possible - extrapolated based on user IP number. The user can opt to choose another location.

As eye-candy, the background picture reflects the weather in current location. The image is pulled from Bing Images based on the weather.

## Compiling

```
mvn install
```

Compilation includes:

* Linting, compiling and building the frontend
* Building and testing the backend

## Running

Several API keys are needed to run the application. The API keys are not distributed as part of the repository.

* OpenWeatherMap https://openweathermap.org/api
* Bing Images https://azure.microsoft.com/en-us/services/cognitive-services/bing-image-search-api/
* Bing Maps https://msdn.microsoft.com/pl-pl/library/ff428642.aspx
* IPStack https://ipstack.com/product

```
export openweather_api_key={{OWM_API_KEY}}
export bing_img_key={{BING_IMG_KEY}}
export bing_map_key={{BING_MAP_KEY}}
export ipstack_key={{IPSTACK_KEY}}

java -jar backend/target/backend-0.0.1-SNAPSHOT.jar
```

An instance of the application is available at https://weather-buildit-ll.herokuapp.com/#/.

## Implementation Notes

The idea behind the application is to provide a complete information with minimum interaction from the user. The application goes to great length to guess the location of the user - Geolocation API is used, and if not available, or times out, the application falls back to IP-based location. The current weather is treated as the most important information, thus it occupies the majority of the screen. Weather forecast is presented as scrollable list below.

While OpenWeatherMap API provides a way to retrieve data based on city name, it is not used because of somewhat limited functionality. Instead, location is determined with Bing maps and the weather data is always retrieved based on geographic coordinates (latitude, longitude).

The design is mobile-centric, and is supposed to look good on a smartphone screen, while staying usable on a PC.

Spring Boot (https://spring.io/projects/spring-boot) and Vue.js (https://vuejs.org/) have been used for implementation. 

Vue.js has been selected as "easier to use and more lightweight" than Angular or React. This was the first attempt to use this framework.

The weather data is obtained from [OpenWeatherMap](https://openweathermap.org/), through Current Weather and Hourly Forecast APIs (free). [OWM JAPI](https://bitbucket.org/aksinghnet/owm-japis) (OpenWeatherMap Java API) is used to get the relevant data.

The location data uses Bing maps API. Bing has been used over Google as the free tier conditions are much better.

The background image is retrieved dynamically from Bing Images API. Again, Bing has been used over Google because of a more relaxed free tier.

## Caveats

Since this is a quickly-made, jury-rigged and generally a demo application, some corners have been cut:

* The OWM JAPI should probably be abandoned due to numerous issues found;
* Unit testing is rather basic and only done for the backend part; with more time, unit tests could be added to the frontend part, possibly with some basic integration tests;
* The frontend part can be developed in isolation by running `npm run dev` in the frontend directory; however, this requires the backend part to be running. With more time, a mock server could be added;
* Gitlab "auto devops" is used to build and test the application; however, it does not really provide much value over standalone unit testing. Heroku deployment from GitLab could be added with more time;
* Error handling is very generic and should be improved;
* The data model itself, and the mapping between source API and the data model is rather basic, and could be improved with Lombok and/or Mapstruct;
* The application is English-only; with some time, it could be localized;
* Late testing discovered that the search box does not accept Polish letters (or non-ASCII characters in general); this is related to how the query strings are created in RestTemplate and would require more testing and additional time to rectify.