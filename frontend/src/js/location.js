import axios from 'axios'

function getLocation () {
  if ('geolocation' in navigator) {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(position => {
        axios.get('api/location/latlon', {
          params: {
            lat: position.coords.latitude,
            lon: position.coords.longitude
          }
        }).then(response => {
          resolve(response.data)
        })
      }, error => {
        console.log(error)
        axios.get('api/location/ip')
          .then(response => {
            resolve(response.data)
          })
      }, {
        timeout: 5000
      })
    })
  } else {
    return new Promise((resolve, reject) => {
      axios.get('api/location/ip')
        .then(response => {
          resolve(response.data)
        })
    })
  }
}

function search (q) {
  return new Promise((resolve, reject) => {
    axios.get('api/location/query', {
      params: {
        query: q
      }
    }).then(response => {
      resolve(response.data)
    })
  })
}

export default {
  getLocation,
  search
}
