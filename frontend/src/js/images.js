import axios from 'axios'

function getBackgroundImage (weather, location) {
  return new Promise((resolve, reject) => {
    axios.get('/api/images/background', {
      params: {
        city: location.city,
        weather: weather.main
      }
    }).then(response => {
      resolve(response.data)
    })
  })
}

export default {
  getBackgroundImage
}
