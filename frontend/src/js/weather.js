import axios from 'axios'

function getCurrentWeather (location) {
  console.log(location)
  return new Promise((resolve, reject) => {
    axios.get('/api/weather/current', {
      params: {
        lat: location.lat,
        lon: location.lon
      }
    }).then(response => {
      resolve(response.data)
    })
  })
}

function getForecast (location) {
  return new Promise((resolve, reject) => {
    axios.get('/api/weather/forecast', {
      params: {
        lat: location.lat,
        lon: location.lon
      }
    }).then(response => {
      resolve(response.data)
    })
  })
}

export default {
  getCurrentWeather,
  getForecast
}
