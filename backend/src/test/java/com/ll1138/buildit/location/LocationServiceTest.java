package com.ll1138.buildit.location;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;

import com.ll1138.buildit.apimock.ApiMockResponse;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

@RunWith(SpringRunner.class)
@RestClientTest(LocationService.class)
public class LocationServiceTest {
    @Autowired
    private LocationService locationService;

    @Autowired
    private MockRestServiceServer mockServer;

    @Test
    public void testGetLocationByLatLon() throws JSONException, IOException {
        mockServer.expect(requestTo(
                "https://dev.virtualearth.net/REST/v1/Locations/10.0,20.0?key=54321"))
                .andRespond(withSuccess(ApiMockResponse.getData("location.json"), MediaType.APPLICATION_JSON));

        Location location = locationService.getLocationByLatLon(10, 20);

        assertEquals(52.2899, location.getLat(), 0.0001);
        assertEquals(20.8328, location.getLon(), 0.0001);
        assertEquals("Izabelin", location.getCity());
        assertEquals("Poland", location.getCountry());
    }

    @Test
    public void getLocationByQuery() throws JSONException, IOException {
        mockServer.expect(requestTo(
                "https://dev.virtualearth.net/REST/v1/Locations/warsaw,poland?key=54321"))
                .andRespond(withSuccess(ApiMockResponse.getData("location.json"), MediaType.APPLICATION_JSON));

        Location location = locationService.getLocationByQuery("warsaw,poland");

        assertEquals(52.2899, location.getLat(), 0.0001);
        assertEquals(20.8328, location.getLon(), 0.0001);
        assertEquals("Izabelin", location.getCity());
        assertEquals("Poland", location.getCountry());
    }

    @Test
    public void testGetLocationByIp() throws JSONException, IOException {
        mockServer.expect(requestTo(
            "http://api.ipstack.com/75.748.86.91?format=1&access_key=65432"))
            .andRespond(withSuccess(ApiMockResponse.getData("ipstack.json"), MediaType.APPLICATION_JSON));

            Location location = locationService.getLocationByIp("75.748.86.91");

            assertEquals(51.3159, location.getLat(), 0.0001);
            assertEquals(20.9887, location.getLon(), 0.0001);
            assertEquals("Oronsko", location.getCity());
            assertEquals("Poland", location.getCountry());
    }


}