package com.ll1138.buildit.weather;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import com.ll1138.buildit.apimock.ApiMockResponse;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.HourlyForecast;
import net.aksingh.owmjapis.OpenWeatherMap;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherServiceTest {

    @MockBean
    OpenWeatherMap owmMock;

    @Autowired
    WeatherService weatherService;

    @Before
    public void setup() throws JSONException, IOException {
        CurrentWeather mockWeather = new OpenWeatherMap("dummy")
                .currentWeatherFromRawResponse(ApiMockResponse.getData("current.json"));

        HourlyForecast mockForecast = new OpenWeatherMap("dummy")
                .hourlyForecastFromRawResponse(ApiMockResponse.getData("forecast.json"));

        Mockito.when(owmMock.currentWeatherByCityName(Mockito.anyString())).thenReturn(mockWeather);
        Mockito.when(owmMock.currentWeatherByCoordinates(Mockito.anyFloat(), Mockito.anyFloat()))
                .thenReturn(mockWeather);
        Mockito.when(owmMock.hourlyForecastByCityName(Mockito.anyString())).thenReturn(mockForecast);
        Mockito.when(owmMock.hourlyForecastByCoordinates(Mockito.anyFloat(), Mockito.anyFloat()))
                .thenReturn(mockForecast);
    }

    @Test
    public void testGetCurrentWeatherByCoords() {
        Weather wthr = weatherService.getCurrentWeather(37.234332396, -115.80666344);

        assertEquals("Clouds", wthr.getMain());
        assertEquals("scattered clouds", wthr.getDescription());
        assertEquals("03n", wthr.getIconId());

        assertEquals(300.15, wthr.getTemperature(), 0.0001f);
        assertEquals(1007, wthr.getPressure(), 0.00001f);
        assertEquals(74, wthr.getHumidity(), 0.00001f);
    }

    @Test
    public void getForecastByCoords() throws IOException {
        List<Weather> fcst = weatherService.getForecast(37.234332396, -115.80666344);

        assertEquals(36, fcst.size());
    }
}