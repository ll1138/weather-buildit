package com.ll1138.buildit.images;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;

import com.ll1138.buildit.apimock.ApiMockResponse;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

@RunWith(SpringRunner.class)
@RestClientTest(ImagesService.class)
public class ImagesServiceTest {

    @Autowired
    private ImagesService imagesService;

    @Autowired
    private MockRestServiceServer mockServer;

    @Test
    public void testGetBackgroundImage() throws JSONException, IOException {

        mockServer.expect(requestTo(
                "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q=weather+sunny&size=large&license=share&aspect=tall&count=1"))
                .andRespond(withSuccess(ApiMockResponse.getData("images.json"), MediaType.APPLICATION_JSON));

        String url = imagesService.getBackgroundImage("warsaw", "sunny");

        assertEquals("https://hydro-zoneinc.com/webstore/images/Warsaw%20Sunny%20Streakless%20gallon.jpg", url);

    }
}