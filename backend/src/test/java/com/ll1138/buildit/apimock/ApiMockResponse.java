package com.ll1138.buildit.apimock;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;

public class ApiMockResponse {
    public static String getData(String id) throws JSONException, IOException {
        return IOUtils.toString(ApiMockResponse.class.getClassLoader().getResourceAsStream("mock/"+id), "UTF-8");
    }
}
