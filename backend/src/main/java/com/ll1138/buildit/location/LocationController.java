package com.ll1138.buildit.location;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/location")
public class LocationController {

    private Logger logger = LoggerFactory.getLogger(LocationController.class);

    @Autowired
    private LocationService locationService;

    /**
     * Return location info based on geographic coordinates
     * 
     * @param lat latitude
     * @param lon longitude
     * @return Location
     */
    @RequestMapping("/latlon")
    public Location getLocationByLatLon(@RequestParam Double lat,@RequestParam Double lon) {
        logger.info("getLocationByLatLon "+lat+";"+lon);
        return locationService.getLocationByLatLon(lat, lon);
    }

    /**
     * Return location info based on freeform query
     * 
     * @param query query
     * @return Location
     */
    @RequestMapping("/query")
    public Location getLocationByQuery(@RequestParam String query) {
        logger.info("getLocationByQuery "+query);
        return locationService.getLocationByQuery(query.replaceAll(" ", "+"));
    }

    /**
     * Return location info based on IP number
     * 
     * @return Location
     */
    @RequestMapping("/ip")
    public Location getLocationByIp(HttpServletRequest request) {

        String ip = request.getRemoteAddr();

        // for local testing only
        if(ip == null || ip.equals("127.0.0.1")) {
            ip = "74.125.224.72";
        }

        logger.info("getLocationByIp "+ip);

        return locationService.getLocationByIp(ip);
    }
}