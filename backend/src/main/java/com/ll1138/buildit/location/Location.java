package com.ll1138.buildit.location;

public class Location {
    private double lat;
    private double lon;
    private String city;
    private String country;

    public Location(Double lat, Double lon, String city, String country) {
        this.lat = lat;
        this.lon = lon;
        this.city = city;
        this.country = country;
    }

    /**
     * @return the lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * @return the lon
     */
    public double getLon() {
        return lon;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }
}