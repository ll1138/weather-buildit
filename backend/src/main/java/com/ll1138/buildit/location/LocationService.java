package com.ll1138.buildit.location;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class LocationService {

    private Logger logger = LoggerFactory.getLogger(LocationService.class);

    private static String BING_MAP_URL = "https://dev.virtualearth.net/REST/v1/Locations/";

    private static String IPSTACK_URL = "http://api.ipstack.com/";

    @Autowired
    private RestTemplate restTemplate;

    @Value("${bing_map_key}")
    private String BING_MAP_KEY;

    @Value("${ipstack_key}")
    private String IPSTACK_KEY;

    /**
     * Return location based on geo coordinates
     * 
     * @param lat latitude
     * @param lon longitude
     * @return location
     */
    public Location getLocationByLatLon(double lat, double lon) {

        return getLocationByQuery(lat + "," + lon);
    }

    /**
     * Return location based on query, using BING maps API
     * 
     * @param query
     * @return location
     */
    public Location getLocationByQuery(String query) {

        logger.info("getLocationByQuery " + query);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BING_MAP_URL).path(query).queryParam("key",
                BING_MAP_KEY);

        JSONObject response = new JSONObject(restTemplate.getForObject(builder.toUriString(), String.class));

        JSONObject r1 = response.getJSONArray("resourceSets").getJSONObject(0).getJSONArray("resources")
                .getJSONObject(0);

        logger.info("getLocationByQuery response " + response);

        return new Location(r1.getJSONObject("point").getJSONArray("coordinates").getDouble(0),
                r1.getJSONObject("point").getJSONArray("coordinates").getDouble(1),
                r1.getJSONObject("address").optString("locality", "Unknown"),
                r1.getJSONObject("address").optString("countryRegion", "Unknown"));
    }

    /**
     * Return location info based on IP number, using the IPStack API
     * 
     * @param ip IP number
     * @return Location
     */
    public Location getLocationByIp(String ip) {

        logger.info("getLocationByIp " + ip);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(IPSTACK_URL).path(ip).queryParam("format", 1)
                .queryParam("access_key", IPSTACK_KEY);

        JSONObject response = new JSONObject(restTemplate.getForObject(builder.toUriString(), String.class));

        logger.info("Location for " + ip + " " + response.toString());

        return new Location(response.getDouble("latitude"), response.getDouble("longitude"),
                response.optString("city", "Unknown"), response.optString("country_name", "Unknown"));
    }
}