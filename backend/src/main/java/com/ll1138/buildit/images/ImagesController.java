package com.ll1138.buildit.images;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/images")
public class ImagesController {

    private Logger logger = LoggerFactory.getLogger(ImagesController.class);

    @Autowired
    private ImagesService imagesService;

    /**
     * Returns a single image URL to represent the city and weather combination
     * 
     * @param city
     * @param weather
     * 
     * @return image URL
     */
    @RequestMapping("/background")
    public String getBackgroundImage(@RequestParam String city, @RequestParam String weather) {
        logger.info("getBackgroundImage "+city+" "+weather);
        return imagesService.getBackgroundImage(city, weather);
    }
}