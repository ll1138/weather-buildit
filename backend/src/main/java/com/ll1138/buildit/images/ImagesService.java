package com.ll1138.buildit.images;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class ImagesService {

    private static final String BING_URL = "https://api.cognitive.microsoft.com/bing/v7.0/images/search";

    private Logger logger = LoggerFactory.getLogger(ImagesService.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${bing_img_key}")
    private String bingImagesKey;

    /**
     * Returns a single image URL to represent the city and weather combination
     * 
     * @param city
     * @param weather
     * 
     * @return image URL
     */
    public String getBackgroundImage(String city, String weather) {

        logger.info("getBackgroundImage " + city + " " + weather);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Ocp-Apim-Subscription-Key", bingImagesKey);

        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BING_URL)
                .queryParam("q", "weather+" + weather).queryParam("size", "large")
                .queryParam("license", "share").queryParam("aspect", "tall").queryParam("count", "1");

        JSONObject response = new JSONObject(
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class).getBody());

        logger.info("getBackgroundImage response " + response);

        return response.getJSONArray("value").getJSONObject(0).getString("contentUrl");
    }
}