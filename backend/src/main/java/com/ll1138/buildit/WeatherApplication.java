package com.ll1138.buildit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import net.aksingh.owmjapis.OpenWeatherMap;
import net.aksingh.owmjapis.OpenWeatherMap.Units;

@SpringBootApplication
public class WeatherApplication {

	@Value("${openweather_api_key}")
    private String apiKey;

	public static void main(String[] args) {
		SpringApplication.run(WeatherApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public OpenWeatherMap openWeatherApi() {
		OpenWeatherMap owm = new OpenWeatherMap(apiKey);
		owm.setUnits(Units.METRIC);
		return owm;
	}
}
