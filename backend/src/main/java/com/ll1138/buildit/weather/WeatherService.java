package com.ll1138.buildit.weather;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.HourlyForecast;
import net.aksingh.owmjapis.OpenWeatherMap;

@Service
public class WeatherService {

    private Logger logger = LoggerFactory.getLogger(WeatherService.class);

    @Autowired
    OpenWeatherMap owm;

    /**
     * Get current weather by geo coords
     * 
     * @param lat latitude
     * @param lon longitude
     * @return Weather
     */
    public Weather getCurrentWeather(double lat, double lon) {
        logger.info("getCurrentWeather "+lat+";"+lon);
        CurrentWeather cw = owm.currentWeatherByCoordinates((float) lat, (float) lon);

        return new Weather(cw);
    }

    /**
     * Get forecast by geo coords
     * 
     * @param lat latitude
     * @param lon longitude
     * @return Weather
     */
    public List<Weather> getForecast(double lat, double lon) {
        logger.info("getForecast "+lat+";"+lon);

        HourlyForecast hf = owm.hourlyForecastByCoordinates((float) lat, (float) lon);

        return Stream.iterate(0, (Integer n) -> n+1).limit(hf.getForecastCount())
        .map((Integer i) -> new Weather(hf.getForecastInstance(i)))
        .collect(Collectors.toList());
    }
}