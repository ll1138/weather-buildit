package com.ll1138.buildit.weather;

import java.util.Date;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.HourlyForecast.Forecast;

public class Weather {
    private String main;
    private String description;
    private String iconId;

    private float temperature;
    private float pressure;
    private float humidity;

    private Date date;

    /**
     * Create Weather object based on openweather CurrentWeather object
     * 
     * @param input
     */
    public Weather(CurrentWeather input) {
        main = input.getWeatherInstance(0).getWeatherName();
        description = input.getWeatherInstance(0).getWeatherDescription();
        iconId = input.getWeatherInstance(0).getWeatherIconName();

        temperature = input.getMainInstance().getTemperature();
        pressure = input.getMainInstance().getPressure();
        humidity = input.getMainInstance().getHumidity();

        date = input.getDateTime();
    }

    /**
     * Create Weather object based on openweather DailyForecast.Forecast data
     * 
     * @param input
     */
    public Weather(Forecast input) {

        main = input.getWeatherInstance(0).getWeatherName();
        description = input.getWeatherInstance(0).getWeatherDescription();
        iconId = input.getWeatherInstance(0).getWeatherIconName();

        temperature = input.getMainInstance().getTemperature();
        pressure = input.getMainInstance().getPressure();
        humidity = input.getMainInstance().getHumidity();

        date = input.getDateTime();
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIconId() {
        return iconId;
    }

    public float getTemperature() {
        return temperature;
    }

    public float getPressure() {
        return pressure;
    }

    public float getHumidity() {
        return humidity;
    }

    public Date getDate() {
        return date;
    }
}