package com.ll1138.buildit.weather;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/weather")
public class WeatherController {

    Logger logger = LoggerFactory.getLogger(WeatherController.class);

    @Autowired
    WeatherService weatherService;

    /**
     * Get current weather by geo coords
     * 
     * @param lat latitude
     * @param lon longitude
     * @return Weather
     */
    @RequestMapping("/current")
    public Weather current(@RequestParam Double lat, @RequestParam Double lon) throws IOException {
        logger.info("current "+lat+";"+lon);
        return weatherService.getCurrentWeather(lat, lon);
    }

    /**
     * Get forecast by geo coords
     * 
     * @param lat latitude
     * @param lon longitude
     * @return Weather
     */
    @RequestMapping("/forecast")
    public List<Weather> forecast(@RequestParam Double lat, @RequestParam Double lon) throws IOException {
        logger.info("forecast "+lat+";"+lon);
        return weatherService.getForecast(lat, lon);
    }
}