# Alternatives for Location

The following is needed from the location service:

* geo coordinates - latitude, longitude
* city
* country

Alternatives:

* Get Lat, Lon from Geolocation API if available -> search address with Bing (see below)
* Get lat, lon, address from ipstack if Geolocation API not available
* Get lat, lon, address from user query

### Search address by lat lon

https://dev.virtualearth.net/REST/v1/Locations/52.289657399999996,20.8324704?includeEntityTypes=Address&inclnb=0&key=BING_MAP_KEY

### Search address by name

https://dev.virtualearth.net/REST/v1/Locations/Warsaw,Poland?includeEntityTypes=Address&inclnb=0&key=BING_MAP_KEY

### Get location from ipstack

http://api.ipstack.com/31.1.81.232?access_key=IPSTACK_KEY&format=1